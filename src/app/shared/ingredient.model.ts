export class Ingredient {
  constructor(
    public name = '',
    public amount = 0,
    public price = 0,
    public icon = '',) {
  }

  getPrice() {
    return this.price = this.price;
  }
}
