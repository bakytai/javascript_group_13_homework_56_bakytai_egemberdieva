import {Component, Input} from '@angular/core';
import {Ingredient} from "../shared/ingredient.model";

@Component({
  selector: 'app-ingredients',
  templateUrl: './ingredients.component.html',
  styleUrls: ['./ingredients.component.css']
})
export class IngredientsComponent {
  @Input() ingredient!: Ingredient;
  ingredientsArray = [
   new Ingredient('Meat', 0, 50,'https://kartinkin.net/uploads/posts/2021-07/1625751848_40-kartinkin-com-p-kusok-myasa-yeda-krasivo-foto-58.jpg') ,
   new Ingredient('Cheese', 0, 20, 'https://m.dom-eda.com/uploads/images/catalog/item/b68db3e52d/a630517a09_1000.png'),
   new Ingredient('Salad', 0 , 5, 'https://eda.ru/img/eda/c464x302/s1.eda.ru/StaticContent/Photos/120304025019/120328130909/p_O.jpg'),
   new Ingredient('Bacon', 0, 30, 'https://idealrecept.com/wp-content/uploads/2019/08/baked-bacon-Lead-1-1300x866.jpg'),
  ];
  toppingArray: string[] = [];

  price = 20;

  constructor() {}

  addItem(index: number) {
    this.toppingArray.push(this.ingredientsArray[index].name);
    this.ingredientsArray[index].amount++;
    const totalPrice = this.ingredientsArray[index].getPrice();
    this.price += totalPrice;
  }

  deleteItem(index: number) {
    if (this.ingredientsArray[index].amount > 0) {
      this.ingredientsArray[index].amount--;
      const totalPrice = this.ingredientsArray[index].getPrice();
      this.price = this.price - totalPrice;
      if (this.toppingArray.includes(this.ingredientsArray[index].name)) {
        const indexOfTopping = this.toppingArray.indexOf(this.ingredientsArray[index].name);
        if (index > -1) {
          this.toppingArray.splice(indexOfTopping, 1);
        }
      }
    } else {
      return
    }
  }

}
