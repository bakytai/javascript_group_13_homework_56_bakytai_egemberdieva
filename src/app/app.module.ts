import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import {FormsModule} from "@angular/forms";
import { IngredientsComponent } from './ingredients/ingredients.component';
import { IngredientItemComponent } from './ingredient-item/ingredient-item.component';


@NgModule({
  declarations: [
    AppComponent,
    IngredientsComponent,
    IngredientItemComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
