import {Component, EventEmitter, Input, Output} from '@angular/core';
import {Ingredient} from "../shared/ingredient.model";

@Component({
  selector: 'app-ingredient-item',
  templateUrl: './ingredient-item.component.html',
  styleUrls: ['./ingredient-item.component.css']
})
export class IngredientItemComponent {
  @Input() ingredient!: Ingredient;
  @Output() add = new EventEmitter();
  @Output() delete = new EventEmitter();
  constructor() {

  }

  addIngredient() {
    this.add.emit();
  }

  deleteIngredient() {
    this.delete.emit();
  }
}
